**EU4 Mass Province Editor** is a GUI tool made for making working with province history files in EU4  a bit less tedious. As the name suggests, many provinces at once can be edited.
Originally created for Equestria Universalis, the tool is now available to everyone.

## Features
Filtering based on:
- Owner tag
- Core tag
- Culture
- Religion
- Base tax
- Base production
- Base manpower
- Total development
- Trade goods
- ID
- Being in HRE

Modifying:
- Owner tag
- Core tag
- Culture
- Religion
- Base tax
- Base production
- Base manpower
- Trade goods
- Being in HRE

## Disclaimer
Make sure to make a backup of your history files before using this tool, otherwise your work might be lost due to bugs in the application.
