﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace eu4_mass_province_editor
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            using (StreamReader sr = new StreamReader("cfg.json"))
            {

                Config cfg = JsonSerializer.Deserialize<Config>(sr.ReadToEnd().ToString());
                this.textBox1.Text = cfg.ModPath;
            }
        }
        public void cfgSave()
        {
            var config = new
            {
                ModPath = this.textBox1.Text,
            };

            var jsonString = JsonSerializer.Serialize(config);
            File.WriteAllText("cfg.json", jsonString);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            List<Prov> provinces = new List<Prov>();
            List<Prov> avProvinces = new List<Prov>();

            try
            {
                foreach (var file in Directory.GetFiles($"{textBox1.Text}\\history\\provinces"))
                    provinces.Add(new Prov(file));
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
                

            if (chkReligion.Checked || chkTradeGood.Checked || chkCulture.Checked || chkTag.Checked || chkTax.Checked || chkProduction.Checked || chkManpower.Checked || chkDevelopment.Checked || chkID.Checked || chkCore.Checked || chkHRE.Checked)
            {
                if ((!String.IsNullOrEmpty(txtReligion.Text) && chkReligion.Checked) || (!String.IsNullOrEmpty(txtTradeGood.Text) && chkTradeGood.Checked) || (!String.IsNullOrEmpty(txtCulture.Text) && chkCulture.Checked) || (!String.IsNullOrEmpty(txtTag.Text) && chkTag.Checked) || (!String.IsNullOrEmpty(txtTax.Text) && chkTax.Checked) || (!String.IsNullOrEmpty(txtProduction.Text) && chkProduction.Checked) || (!String.IsNullOrEmpty(txtManpower.Text) && chkManpower.Checked) || (!String.IsNullOrEmpty(txtDevelopment.Text) && chkDevelopment.Checked) || (!String.IsNullOrEmpty(txtID.Text) && chkID.Checked) || (!String.IsNullOrEmpty(txtCore.Text) && chkCore.Checked) || (!String.IsNullOrEmpty(txtHRE.Text) && chkHRE.Checked))
                    avProvinces = provinces;
                else
                    MessageBox.Show("Fields must not be empty.");
            }

            else
                MessageBox.Show("Please check at least one box first.");

            if (chkID.Checked)
            {
                try
                {
                    avProvinces = avProvinces.Where(p => p.ID == txtID.Text).ToList();
                }
                catch (FormatException)
                {
                    Console.WriteLine("Error!");
                    return;
                }

            }
            if (chkReligion.Checked)
                avProvinces = avProvinces.Where(p => p.Religion == txtReligion.Text).ToList();
            if (chkTradeGood.Checked)
                avProvinces = avProvinces.Where(p => p.TradeGood == txtTradeGood.Text).ToList();
            if (chkCulture.Checked)
                avProvinces = avProvinces.Where(p => p.Culture == txtCulture.Text).ToList();
            if (chkTag.Checked)
                avProvinces = avProvinces.Where(p => p.Owner == txtTag.Text).ToList();
            if (chkTax.Checked)
                avProvinces = avProvinces.Where(p => p.BaseTax.ToString() == txtTax.Text).ToList();
            if (chkProduction.Checked)
                avProvinces = avProvinces.Where(p => p.BaseProduction.ToString() == txtProduction.Text).ToList();
            if (chkManpower.Checked)
                avProvinces = avProvinces.Where(p => p.BaseManpower.ToString() == txtManpower.Text).ToList();
            if (chkDevelopment.Checked)
                avProvinces = avProvinces.Where(p => (p.BaseManpower + p.BaseProduction + p.BaseTax).ToString() == txtDevelopment.Text).ToList();
            if (chkHRE.Checked)
                avProvinces = avProvinces.Where(p => p.HRE == txtHRE.Text).ToList();

            foreach (Prov province in avProvinces)
            {
                string entireFile = "";
                string[] line = { "" };

                using (StreamReader sr = new StreamReader(province.File))
                {
                    entireFile = sr.ReadToEnd();
                    line = entireFile.Split('\n');

                    for(int i = 0; i <= line.Length - 1; i++)
                    {
                        if(line[i] != null && !line[i].StartsWith("#"))
                        {
                            if (chkTag_edit.Checked && line[i].Contains("owner = "))
                                line[i] = $"owner = {txtTag_edit.Text}";

                            if (chkCore_edit.Checked && line[i].Contains("add_core = "))
                                line[i] = $"add_core = {txtCore_edit.Text}";

                            if (chkCulture_edit.Checked && line[i].Contains("culture = "))
                                line[i] = $"culture = {txtCulture_edit.Text}";

                            if (chkReligion_edit.Checked && line[i].Contains("religion = "))
                                line[i] = $"religion = {txtReligion_edit.Text}";

                            if (chkTax_edit.Checked && line[i].Contains("base_tax = "))
                                line[i] = $"base_tax = {txtTag_edit.Text}";

                            if (chkProduction.Checked && line[i].Contains("base_production = "))
                                line[i] = $"base_production = {txtProduction_edit.Text}";

                            if (chkManpower_edit.Checked && line[i].Contains("base_manpower = "))
                                line[i] = $"base_manpower = {txtManpower_edit.Text}";

                            if (chkTradeGood_edit.Checked && line[i].Contains("trade_goods = "))
                                line[i] = $"trade_goods = {txtTradeGood_edit.Text}";

                            if (chkHRE_edit.Checked && line[i].Contains("hre = "))
                                line[i] = $"hre = {txtHRE_edit.Text}";
                        }
                    }
                    sr.Close();
                }

                string newFile = "";
                foreach(string aline in line)
                {
                    if(!String.IsNullOrEmpty(aline))
                        newFile += $"{aline}\n";
                }
                File.WriteAllText(province.File, newFile);

                Console.WriteLine($"{province.ID} has been modified.");
                txtLogs.AppendText($"{province.ID} has been modified.");
                txtLogs.AppendText(Environment.NewLine);
            }
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
            cfgSave();
        }

        private void txtTag_Leave(object sender, EventArgs e)
        {
            txtTag.Text = txtTag.Text.ToUpper();
        }

        private void txtTag_edit_Leave(object sender, EventArgs e)
        {
            txtTag_edit.Text = txtTag_edit.Text.ToUpper();

        }

        private void openFolder_Click(object sender, EventArgs e)
        {
            OpenFileDialog folderBrowser = new OpenFileDialog();
            folderBrowser.ValidateNames = false;
            folderBrowser.CheckFileExists = false;
            folderBrowser.CheckPathExists = true;
            folderBrowser.FileName = "Select folder";
            if (folderBrowser.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = Path.GetDirectoryName(folderBrowser.FileName);
                cfgSave();
            }
        }

        private void txtCore_Leave(object sender, EventArgs e)
        {
            txtCore.Text = txtCore.Text.ToUpper();
        }

        private void txtCore_edit_Leave(object sender, EventArgs e)
        {
            txtCore_edit.Text = txtCore_edit.Text.ToUpper();
        }
    }


}
