﻿namespace eu4_mass_province_editor
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lblModPath = new System.Windows.Forms.Label();
            this.btnRun = new System.Windows.Forms.Button();
            this.chkTag = new System.Windows.Forms.CheckBox();
            this.chkCulture = new System.Windows.Forms.CheckBox();
            this.chkReligion = new System.Windows.Forms.CheckBox();
            this.chkTax = new System.Windows.Forms.CheckBox();
            this.chkProduction = new System.Windows.Forms.CheckBox();
            this.chkManpower = new System.Windows.Forms.CheckBox();
            this.chkTradeGood = new System.Windows.Forms.CheckBox();
            this.chkID = new System.Windows.Forms.CheckBox();
            this.txtID = new System.Windows.Forms.TextBox();
            this.txtProduction = new System.Windows.Forms.NumericUpDown();
            this.chkDevelopment = new System.Windows.Forms.CheckBox();
            this.txtManpower = new System.Windows.Forms.NumericUpDown();
            this.txtDevelopment = new System.Windows.Forms.NumericUpDown();
            this.txtTax = new System.Windows.Forms.NumericUpDown();
            this.txtReligion = new System.Windows.Forms.TextBox();
            this.txtCulture = new System.Windows.Forms.TextBox();
            this.txtTag = new System.Windows.Forms.TextBox();
            this.txtTradeGood = new System.Windows.Forms.TextBox();
            this.txtTradeGood_edit = new System.Windows.Forms.TextBox();
            this.txtTag_edit = new System.Windows.Forms.TextBox();
            this.txtCulture_edit = new System.Windows.Forms.TextBox();
            this.txtReligion_edit = new System.Windows.Forms.TextBox();
            this.txtTax_edit = new System.Windows.Forms.NumericUpDown();
            this.txtManpower_edit = new System.Windows.Forms.NumericUpDown();
            this.txtProduction_edit = new System.Windows.Forms.NumericUpDown();
            this.chkTradeGood_edit = new System.Windows.Forms.CheckBox();
            this.chkManpower_edit = new System.Windows.Forms.CheckBox();
            this.chkProduction_edit = new System.Windows.Forms.CheckBox();
            this.chkTax_edit = new System.Windows.Forms.CheckBox();
            this.chkReligion_edit = new System.Windows.Forms.CheckBox();
            this.chkCulture_edit = new System.Windows.Forms.CheckBox();
            this.chkTag_edit = new System.Windows.Forms.CheckBox();
            this.chkRandom = new System.Windows.Forms.CheckBox();
            this.txtLogs = new System.Windows.Forms.TextBox();
            this.openFolder = new System.Windows.Forms.Button();
            this.txtCore_edit = new System.Windows.Forms.TextBox();
            this.chkCore_edit = new System.Windows.Forms.CheckBox();
            this.txtCore = new System.Windows.Forms.TextBox();
            this.chkCore = new System.Windows.Forms.CheckBox();
            this.lblAffectedAmount = new System.Windows.Forms.Label();
            this.chkHRE_edit = new System.Windows.Forms.CheckBox();
            this.chkHRE = new System.Windows.Forms.CheckBox();
            this.txtHRE = new System.Windows.Forms.ComboBox();
            this.txtHRE_edit = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtProduction)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtManpower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDevelopment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTax_edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtManpower_edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProduction_edit)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(71, 6);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(214, 20);
            this.textBox1.TabIndex = 0;
            this.textBox1.Leave += new System.EventHandler(this.textBox1_Leave);
            // 
            // lblModPath
            // 
            this.lblModPath.AutoSize = true;
            this.lblModPath.Location = new System.Drawing.Point(9, 9);
            this.lblModPath.Name = "lblModPath";
            this.lblModPath.Size = new System.Drawing.Size(56, 13);
            this.lblModPath.TabIndex = 3;
            this.lblModPath.Text = "Mod Path:";
            // 
            // btnRun
            // 
            this.btnRun.Location = new System.Drawing.Point(713, 420);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(75, 23);
            this.btnRun.TabIndex = 4;
            this.btnRun.Text = "Run";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.button1_Click);
            // 
            // chkTag
            // 
            this.chkTag.AutoSize = true;
            this.chkTag.Location = new System.Drawing.Point(12, 37);
            this.chkTag.Name = "chkTag";
            this.chkTag.Size = new System.Drawing.Size(48, 17);
            this.chkTag.TabIndex = 5;
            this.chkTag.Text = "Tag:";
            this.chkTag.UseVisualStyleBackColor = true;
            // 
            // chkCulture
            // 
            this.chkCulture.AutoSize = true;
            this.chkCulture.Location = new System.Drawing.Point(12, 83);
            this.chkCulture.Name = "chkCulture";
            this.chkCulture.Size = new System.Drawing.Size(62, 17);
            this.chkCulture.TabIndex = 6;
            this.chkCulture.Text = "Culture:";
            this.chkCulture.UseVisualStyleBackColor = true;
            // 
            // chkReligion
            // 
            this.chkReligion.AutoSize = true;
            this.chkReligion.Location = new System.Drawing.Point(12, 106);
            this.chkReligion.Name = "chkReligion";
            this.chkReligion.Size = new System.Drawing.Size(67, 17);
            this.chkReligion.TabIndex = 7;
            this.chkReligion.Text = "Religion:";
            this.chkReligion.UseVisualStyleBackColor = true;
            // 
            // chkTax
            // 
            this.chkTax.AutoSize = true;
            this.chkTax.Location = new System.Drawing.Point(12, 129);
            this.chkTax.Name = "chkTax";
            this.chkTax.Size = new System.Drawing.Size(74, 17);
            this.chkTax.TabIndex = 8;
            this.chkTax.Text = "Base Tax:";
            this.chkTax.UseVisualStyleBackColor = true;
            // 
            // chkProduction
            // 
            this.chkProduction.AutoSize = true;
            this.chkProduction.Location = new System.Drawing.Point(12, 152);
            this.chkProduction.Name = "chkProduction";
            this.chkProduction.Size = new System.Drawing.Size(107, 17);
            this.chkProduction.TabIndex = 9;
            this.chkProduction.Text = "Base Production:";
            this.chkProduction.UseVisualStyleBackColor = true;
            // 
            // chkManpower
            // 
            this.chkManpower.AutoSize = true;
            this.chkManpower.Location = new System.Drawing.Point(12, 175);
            this.chkManpower.Name = "chkManpower";
            this.chkManpower.Size = new System.Drawing.Size(106, 17);
            this.chkManpower.TabIndex = 10;
            this.chkManpower.Text = "Base Manpower:";
            this.chkManpower.UseVisualStyleBackColor = true;
            // 
            // chkTradeGood
            // 
            this.chkTradeGood.AutoSize = true;
            this.chkTradeGood.Location = new System.Drawing.Point(12, 221);
            this.chkTradeGood.Name = "chkTradeGood";
            this.chkTradeGood.Size = new System.Drawing.Size(86, 17);
            this.chkTradeGood.TabIndex = 11;
            this.chkTradeGood.Text = "Trade Good:";
            this.chkTradeGood.UseVisualStyleBackColor = true;
            // 
            // chkID
            // 
            this.chkID.AutoSize = true;
            this.chkID.Location = new System.Drawing.Point(12, 244);
            this.chkID.Name = "chkID";
            this.chkID.Size = new System.Drawing.Size(51, 17);
            this.chkID.TabIndex = 12;
            this.chkID.Text = "ID(s):";
            this.chkID.UseVisualStyleBackColor = true;
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(137, 242);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(148, 20);
            this.txtID.TabIndex = 13;
            // 
            // txtProduction
            // 
            this.txtProduction.Location = new System.Drawing.Point(137, 152);
            this.txtProduction.Name = "txtProduction";
            this.txtProduction.Size = new System.Drawing.Size(71, 20);
            this.txtProduction.TabIndex = 14;
            // 
            // chkDevelopment
            // 
            this.chkDevelopment.AutoSize = true;
            this.chkDevelopment.Location = new System.Drawing.Point(12, 198);
            this.chkDevelopment.Name = "chkDevelopment";
            this.chkDevelopment.Size = new System.Drawing.Size(119, 17);
            this.chkDevelopment.TabIndex = 15;
            this.chkDevelopment.Text = "Total Development:";
            this.chkDevelopment.UseVisualStyleBackColor = true;
            // 
            // txtManpower
            // 
            this.txtManpower.Location = new System.Drawing.Point(137, 174);
            this.txtManpower.Name = "txtManpower";
            this.txtManpower.Size = new System.Drawing.Size(71, 20);
            this.txtManpower.TabIndex = 16;
            // 
            // txtDevelopment
            // 
            this.txtDevelopment.Location = new System.Drawing.Point(137, 197);
            this.txtDevelopment.Name = "txtDevelopment";
            this.txtDevelopment.Size = new System.Drawing.Size(71, 20);
            this.txtDevelopment.TabIndex = 17;
            // 
            // txtTax
            // 
            this.txtTax.Location = new System.Drawing.Point(137, 128);
            this.txtTax.Name = "txtTax";
            this.txtTax.Size = new System.Drawing.Size(71, 20);
            this.txtTax.TabIndex = 18;
            // 
            // txtReligion
            // 
            this.txtReligion.Location = new System.Drawing.Point(137, 106);
            this.txtReligion.Name = "txtReligion";
            this.txtReligion.Size = new System.Drawing.Size(148, 20);
            this.txtReligion.TabIndex = 19;
            // 
            // txtCulture
            // 
            this.txtCulture.Location = new System.Drawing.Point(137, 83);
            this.txtCulture.Name = "txtCulture";
            this.txtCulture.Size = new System.Drawing.Size(148, 20);
            this.txtCulture.TabIndex = 20;
            // 
            // txtTag
            // 
            this.txtTag.Location = new System.Drawing.Point(137, 37);
            this.txtTag.MaxLength = 3;
            this.txtTag.Name = "txtTag";
            this.txtTag.Size = new System.Drawing.Size(148, 20);
            this.txtTag.TabIndex = 21;
            this.txtTag.Leave += new System.EventHandler(this.txtTag_Leave);
            // 
            // txtTradeGood
            // 
            this.txtTradeGood.Location = new System.Drawing.Point(137, 219);
            this.txtTradeGood.Name = "txtTradeGood";
            this.txtTradeGood.Size = new System.Drawing.Size(148, 20);
            this.txtTradeGood.TabIndex = 22;
            // 
            // txtTradeGood_edit
            // 
            this.txtTradeGood_edit.Location = new System.Drawing.Point(486, 198);
            this.txtTradeGood_edit.Name = "txtTradeGood_edit";
            this.txtTradeGood_edit.Size = new System.Drawing.Size(148, 20);
            this.txtTradeGood_edit.TabIndex = 38;
            // 
            // txtTag_edit
            // 
            this.txtTag_edit.Location = new System.Drawing.Point(486, 37);
            this.txtTag_edit.MaxLength = 3;
            this.txtTag_edit.Name = "txtTag_edit";
            this.txtTag_edit.Size = new System.Drawing.Size(148, 20);
            this.txtTag_edit.TabIndex = 37;
            this.txtTag_edit.Leave += new System.EventHandler(this.txtTag_edit_Leave);
            // 
            // txtCulture_edit
            // 
            this.txtCulture_edit.Location = new System.Drawing.Point(486, 81);
            this.txtCulture_edit.Name = "txtCulture_edit";
            this.txtCulture_edit.Size = new System.Drawing.Size(148, 20);
            this.txtCulture_edit.TabIndex = 36;
            // 
            // txtReligion_edit
            // 
            this.txtReligion_edit.Location = new System.Drawing.Point(486, 106);
            this.txtReligion_edit.Name = "txtReligion_edit";
            this.txtReligion_edit.Size = new System.Drawing.Size(148, 20);
            this.txtReligion_edit.TabIndex = 35;
            // 
            // txtTax_edit
            // 
            this.txtTax_edit.Location = new System.Drawing.Point(486, 129);
            this.txtTax_edit.Name = "txtTax_edit";
            this.txtTax_edit.Size = new System.Drawing.Size(71, 20);
            this.txtTax_edit.TabIndex = 34;
            // 
            // txtManpower_edit
            // 
            this.txtManpower_edit.Location = new System.Drawing.Point(486, 174);
            this.txtManpower_edit.Name = "txtManpower_edit";
            this.txtManpower_edit.Size = new System.Drawing.Size(71, 20);
            this.txtManpower_edit.TabIndex = 32;
            // 
            // txtProduction_edit
            // 
            this.txtProduction_edit.Location = new System.Drawing.Point(486, 151);
            this.txtProduction_edit.Name = "txtProduction_edit";
            this.txtProduction_edit.Size = new System.Drawing.Size(71, 20);
            this.txtProduction_edit.TabIndex = 30;
            // 
            // chkTradeGood_edit
            // 
            this.chkTradeGood_edit.AutoSize = true;
            this.chkTradeGood_edit.Location = new System.Drawing.Point(361, 203);
            this.chkTradeGood_edit.Name = "chkTradeGood_edit";
            this.chkTradeGood_edit.Size = new System.Drawing.Size(86, 17);
            this.chkTradeGood_edit.TabIndex = 29;
            this.chkTradeGood_edit.Text = "Trade Good:";
            this.chkTradeGood_edit.UseVisualStyleBackColor = true;
            // 
            // chkManpower_edit
            // 
            this.chkManpower_edit.AutoSize = true;
            this.chkManpower_edit.Location = new System.Drawing.Point(361, 181);
            this.chkManpower_edit.Name = "chkManpower_edit";
            this.chkManpower_edit.Size = new System.Drawing.Size(106, 17);
            this.chkManpower_edit.TabIndex = 28;
            this.chkManpower_edit.Text = "Base Manpower:";
            this.chkManpower_edit.UseVisualStyleBackColor = true;
            // 
            // chkProduction_edit
            // 
            this.chkProduction_edit.AutoSize = true;
            this.chkProduction_edit.Location = new System.Drawing.Point(361, 158);
            this.chkProduction_edit.Name = "chkProduction_edit";
            this.chkProduction_edit.Size = new System.Drawing.Size(107, 17);
            this.chkProduction_edit.TabIndex = 27;
            this.chkProduction_edit.Text = "Base Production:";
            this.chkProduction_edit.UseVisualStyleBackColor = true;
            // 
            // chkTax_edit
            // 
            this.chkTax_edit.AutoSize = true;
            this.chkTax_edit.Location = new System.Drawing.Point(361, 135);
            this.chkTax_edit.Name = "chkTax_edit";
            this.chkTax_edit.Size = new System.Drawing.Size(74, 17);
            this.chkTax_edit.TabIndex = 26;
            this.chkTax_edit.Text = "Base Tax:";
            this.chkTax_edit.UseVisualStyleBackColor = true;
            // 
            // chkReligion_edit
            // 
            this.chkReligion_edit.AutoSize = true;
            this.chkReligion_edit.Location = new System.Drawing.Point(361, 112);
            this.chkReligion_edit.Name = "chkReligion_edit";
            this.chkReligion_edit.Size = new System.Drawing.Size(67, 17);
            this.chkReligion_edit.TabIndex = 25;
            this.chkReligion_edit.Text = "Religion:";
            this.chkReligion_edit.UseVisualStyleBackColor = true;
            // 
            // chkCulture_edit
            // 
            this.chkCulture_edit.AutoSize = true;
            this.chkCulture_edit.Location = new System.Drawing.Point(361, 89);
            this.chkCulture_edit.Name = "chkCulture_edit";
            this.chkCulture_edit.Size = new System.Drawing.Size(62, 17);
            this.chkCulture_edit.TabIndex = 24;
            this.chkCulture_edit.Text = "Culture:";
            this.chkCulture_edit.UseVisualStyleBackColor = true;
            // 
            // chkTag_edit
            // 
            this.chkTag_edit.AutoSize = true;
            this.chkTag_edit.Location = new System.Drawing.Point(361, 37);
            this.chkTag_edit.Name = "chkTag_edit";
            this.chkTag_edit.Size = new System.Drawing.Size(48, 17);
            this.chkTag_edit.TabIndex = 23;
            this.chkTag_edit.Text = "Tag:";
            this.chkTag_edit.UseVisualStyleBackColor = true;
            // 
            // chkRandom
            // 
            this.chkRandom.AutoSize = true;
            this.chkRandom.Location = new System.Drawing.Point(576, 158);
            this.chkRandom.Name = "chkRandom";
            this.chkRandom.Size = new System.Drawing.Size(85, 17);
            this.chkRandom.TabIndex = 39;
            this.chkRandom.Text = "Randomize?";
            this.chkRandom.UseVisualStyleBackColor = true;
            this.chkRandom.Visible = false;
            // 
            // txtLogs
            // 
            this.txtLogs.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtLogs.Location = new System.Drawing.Point(361, 249);
            this.txtLogs.MaxLength = 2147483647;
            this.txtLogs.Multiline = true;
            this.txtLogs.Name = "txtLogs";
            this.txtLogs.ReadOnly = true;
            this.txtLogs.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLogs.Size = new System.Drawing.Size(427, 165);
            this.txtLogs.TabIndex = 40;
            // 
            // openFolder
            // 
            this.openFolder.Location = new System.Drawing.Point(291, 3);
            this.openFolder.Name = "openFolder";
            this.openFolder.Size = new System.Drawing.Size(34, 23);
            this.openFolder.TabIndex = 41;
            this.openFolder.Text = "...";
            this.openFolder.UseVisualStyleBackColor = true;
            this.openFolder.Click += new System.EventHandler(this.openFolder_Click);
            // 
            // txtCore_edit
            // 
            this.txtCore_edit.Location = new System.Drawing.Point(486, 59);
            this.txtCore_edit.Name = "txtCore_edit";
            this.txtCore_edit.Size = new System.Drawing.Size(148, 20);
            this.txtCore_edit.TabIndex = 43;
            this.txtCore_edit.Leave += new System.EventHandler(this.txtCore_edit_Leave);
            // 
            // chkCore_edit
            // 
            this.chkCore_edit.AutoSize = true;
            this.chkCore_edit.Location = new System.Drawing.Point(361, 64);
            this.chkCore_edit.Name = "chkCore_edit";
            this.chkCore_edit.Size = new System.Drawing.Size(51, 17);
            this.chkCore_edit.TabIndex = 42;
            this.chkCore_edit.Text = "Core:";
            this.chkCore_edit.UseVisualStyleBackColor = true;
            // 
            // txtCore
            // 
            this.txtCore.Location = new System.Drawing.Point(137, 59);
            this.txtCore.Name = "txtCore";
            this.txtCore.Size = new System.Drawing.Size(148, 20);
            this.txtCore.TabIndex = 45;
            this.txtCore.Leave += new System.EventHandler(this.txtCore_Leave);
            // 
            // chkCore
            // 
            this.chkCore.AutoSize = true;
            this.chkCore.Location = new System.Drawing.Point(12, 60);
            this.chkCore.Name = "chkCore";
            this.chkCore.Size = new System.Drawing.Size(51, 17);
            this.chkCore.TabIndex = 44;
            this.chkCore.Text = "Core:";
            this.chkCore.UseVisualStyleBackColor = true;
            // 
            // lblAffectedAmount
            // 
            this.lblAffectedAmount.AutoSize = true;
            this.lblAffectedAmount.Location = new System.Drawing.Point(358, 420);
            this.lblAffectedAmount.Name = "lblAffectedAmount";
            this.lblAffectedAmount.Size = new System.Drawing.Size(0, 13);
            this.lblAffectedAmount.TabIndex = 46;
            // 
            // chkHRE_edit
            // 
            this.chkHRE_edit.AutoSize = true;
            this.chkHRE_edit.Location = new System.Drawing.Point(361, 226);
            this.chkHRE_edit.Name = "chkHRE_edit";
            this.chkHRE_edit.Size = new System.Drawing.Size(64, 17);
            this.chkHRE_edit.TabIndex = 47;
            this.chkHRE_edit.Text = "In HRE:";
            this.chkHRE_edit.UseVisualStyleBackColor = true;
            // 
            // chkHRE
            // 
            this.chkHRE.AutoSize = true;
            this.chkHRE.Location = new System.Drawing.Point(12, 267);
            this.chkHRE.Name = "chkHRE";
            this.chkHRE.Size = new System.Drawing.Size(64, 17);
            this.chkHRE.TabIndex = 49;
            this.chkHRE.Text = "In HRE:";
            this.chkHRE.UseVisualStyleBackColor = true;
            // 
            // txtHRE
            // 
            this.txtHRE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtHRE.FormattingEnabled = true;
            this.txtHRE.Items.AddRange(new object[] {
            "yes",
            "no"});
            this.txtHRE.Location = new System.Drawing.Point(137, 265);
            this.txtHRE.Name = "txtHRE";
            this.txtHRE.Size = new System.Drawing.Size(121, 21);
            this.txtHRE.TabIndex = 50;
            // 
            // txtHRE_edit
            // 
            this.txtHRE_edit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtHRE_edit.FormattingEnabled = true;
            this.txtHRE_edit.Items.AddRange(new object[] {
            "yes",
            "no"});
            this.txtHRE_edit.Location = new System.Drawing.Point(486, 224);
            this.txtHRE_edit.Name = "txtHRE_edit";
            this.txtHRE_edit.Size = new System.Drawing.Size(121, 21);
            this.txtHRE_edit.TabIndex = 51;
            // 
            // Form1
            // 
            this.AcceptButton = this.btnRun;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.txtHRE_edit);
            this.Controls.Add(this.txtHRE);
            this.Controls.Add(this.chkHRE);
            this.Controls.Add(this.chkHRE_edit);
            this.Controls.Add(this.lblAffectedAmount);
            this.Controls.Add(this.txtCore);
            this.Controls.Add(this.chkCore);
            this.Controls.Add(this.txtCore_edit);
            this.Controls.Add(this.chkCore_edit);
            this.Controls.Add(this.openFolder);
            this.Controls.Add(this.txtLogs);
            this.Controls.Add(this.chkRandom);
            this.Controls.Add(this.txtTradeGood_edit);
            this.Controls.Add(this.txtTag_edit);
            this.Controls.Add(this.txtCulture_edit);
            this.Controls.Add(this.txtReligion_edit);
            this.Controls.Add(this.txtTax_edit);
            this.Controls.Add(this.txtManpower_edit);
            this.Controls.Add(this.txtProduction_edit);
            this.Controls.Add(this.chkTradeGood_edit);
            this.Controls.Add(this.chkManpower_edit);
            this.Controls.Add(this.chkProduction_edit);
            this.Controls.Add(this.chkTax_edit);
            this.Controls.Add(this.chkReligion_edit);
            this.Controls.Add(this.chkCulture_edit);
            this.Controls.Add(this.chkTag_edit);
            this.Controls.Add(this.txtTradeGood);
            this.Controls.Add(this.txtTag);
            this.Controls.Add(this.txtCulture);
            this.Controls.Add(this.txtReligion);
            this.Controls.Add(this.txtTax);
            this.Controls.Add(this.txtDevelopment);
            this.Controls.Add(this.txtManpower);
            this.Controls.Add(this.chkDevelopment);
            this.Controls.Add(this.txtProduction);
            this.Controls.Add(this.txtID);
            this.Controls.Add(this.chkID);
            this.Controls.Add(this.chkTradeGood);
            this.Controls.Add(this.chkManpower);
            this.Controls.Add(this.chkProduction);
            this.Controls.Add(this.chkTax);
            this.Controls.Add(this.chkReligion);
            this.Controls.Add(this.chkCulture);
            this.Controls.Add(this.chkTag);
            this.Controls.Add(this.btnRun);
            this.Controls.Add(this.lblModPath);
            this.Controls.Add(this.textBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.txtProduction)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtManpower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDevelopment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTax_edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtManpower_edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProduction_edit)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lblModPath;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.CheckBox chkTag;
        private System.Windows.Forms.CheckBox chkCulture;
        private System.Windows.Forms.CheckBox chkReligion;
        private System.Windows.Forms.CheckBox chkTax;
        private System.Windows.Forms.CheckBox chkProduction;
        private System.Windows.Forms.CheckBox chkManpower;
        private System.Windows.Forms.CheckBox chkTradeGood;
        private System.Windows.Forms.CheckBox chkID;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.NumericUpDown txtProduction;
        private System.Windows.Forms.CheckBox chkDevelopment;
        private System.Windows.Forms.NumericUpDown txtManpower;
        private System.Windows.Forms.NumericUpDown txtDevelopment;
        private System.Windows.Forms.NumericUpDown txtTax;
        private System.Windows.Forms.TextBox txtReligion;
        private System.Windows.Forms.TextBox txtCulture;
        private System.Windows.Forms.TextBox txtTag;
        private System.Windows.Forms.TextBox txtTradeGood;
        private System.Windows.Forms.TextBox txtTradeGood_edit;
        private System.Windows.Forms.TextBox txtTag_edit;
        private System.Windows.Forms.TextBox txtCulture_edit;
        private System.Windows.Forms.TextBox txtReligion_edit;
        private System.Windows.Forms.NumericUpDown txtTax_edit;
        private System.Windows.Forms.NumericUpDown txtManpower_edit;
        private System.Windows.Forms.NumericUpDown txtProduction_edit;
        private System.Windows.Forms.CheckBox chkTradeGood_edit;
        private System.Windows.Forms.CheckBox chkManpower_edit;
        private System.Windows.Forms.CheckBox chkProduction_edit;
        private System.Windows.Forms.CheckBox chkTax_edit;
        private System.Windows.Forms.CheckBox chkReligion_edit;
        private System.Windows.Forms.CheckBox chkCulture_edit;
        private System.Windows.Forms.CheckBox chkTag_edit;
        private System.Windows.Forms.CheckBox chkRandom;
        private System.Windows.Forms.TextBox txtLogs;
        private System.Windows.Forms.Button openFolder;
        private System.Windows.Forms.TextBox txtCore_edit;
        private System.Windows.Forms.CheckBox chkCore_edit;
        private System.Windows.Forms.TextBox txtCore;
        private System.Windows.Forms.CheckBox chkCore;
        private System.Windows.Forms.Label lblAffectedAmount;
        private System.Windows.Forms.CheckBox chkHRE_edit;
        private System.Windows.Forms.CheckBox chkHRE;
        private System.Windows.Forms.ComboBox txtHRE;
        private System.Windows.Forms.ComboBox txtHRE_edit;
    }
}

