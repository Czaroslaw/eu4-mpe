﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace eu4_mass_province_editor
{
    class Prov
    {
        public string File { get; set; }
        public string ID { get; set; }
        public string Owner { get; set; }
        public string Controller { get; set; }
        public string HRE { get; set; }
        public string[] Core { get; set; }
        public string TradeGood { get; set; }
        public string Religion { get; set; }
        public string Culture { get; set; }
        public int BaseTax { get; set; }
        public int BaseProduction { get; set; }
        public int BaseManpower { get; set; }
        public int Development { get; set; }

        public Prov(string path)
        {
            File = path;
            string[] pathFragments = path.Split('\\');
            string[] nameFragments = pathFragments[pathFragments.Length - 1].Split(' ', '-', '.');
            ID = nameFragments[0];

            using (StreamReader sr = new StreamReader(path))
            {
                string line;
                while((line = sr.ReadLine()) != null && (!line.StartsWith("#")))
                {
                    //if (line.Contains("add_core = "))
                        //this.Core[0] = line.Split(new string[] { "add_core = " }, StringSplitOptions.None)[1];
                    if (line.Contains("owner = "))
                        this.Owner = line.Split(new string[] { "owner = " }, StringSplitOptions.None)[1];
                    if (line.Contains("controller = "))
                        this.Controller = line.Split(new string[] { "controller = " }, StringSplitOptions.None)[1];
                    if (line.Contains("trade_goods = "))
                        this.TradeGood = line.Split(new string[] { "trade_goods = " }, StringSplitOptions.None)[1];
                    if (line.Contains("culture = "))
                        this.Culture = line.Split(new string[] { "culture = " }, StringSplitOptions.None)[1];
                    if (line.Contains("religion = "))
                        this.Religion = line.Split(new string[] { "religion = " }, StringSplitOptions.None)[1];
                    if (line.Contains("base_tax = "))
                        this.BaseTax = int.Parse(line.Split(new string[] { "base_tax = " }, StringSplitOptions.None)[1]);
                    if (line.Contains("base_production = "))
                        this.BaseProduction = int.Parse(line.Split(new string[] { "base_production = " }, StringSplitOptions.None)[1]);
                    if (line.Contains("base_manpower = "))
                        this.BaseManpower = int.Parse(line.Split(new string[] { "base_manpower = " }, StringSplitOptions.None)[1]);
                    if (line.Contains("hre = "))
                        this.HRE = line.Split(new string[] { "hre = " }, StringSplitOptions.None)[1];
                }
                sr.Close();
            }
        }

        public override string ToString()
        {
            return ($"{File} | {ID}");
        }
    }
}
